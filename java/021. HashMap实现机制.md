>1、 HashMap基于哈希表的 Map 接口的实现。此实现提供所有可选的映射操作，并允许使用 null 值和 null键。（除了不同步和允许使用 null 之外，HashMap 类与 Hashtable大致相同）
> 2、此类不保证映射的顺序，特别是它不保证该顺序恒久不变。
>3、 值得注意的是HashMap不是线程安全的，如果想要线程安全的HashMap，可以通过Collections类的静态方法synchronizedMap获得线程安全的HashMap。
 Map map = Collections.synchronizedMap(new HashMap());

同样，还是列出HashMap的属性参数！

```java
   //树化链表节点的阈值，当某个链表的长度大于或者等于这个长度，则扩大数组容量，或者数化链表  
   static final int TREEIFY_THRESHOLD = 8;  
   //初始容量，必须是2的倍数，默认是16  
   static final int DEFAULT_INITIAL_CAPACITY = 1 << 4; // aka 16  
   //最大所能容纳的key-value 个数  
   static final int MAXIMUM_CAPACITY = 1 << 30;  
   //默认的加载因子  
   static final float DEFAULT_LOAD_FACTOR = 0.75f;  
   //存储数据的Node数组，长度是2的幂。  
   transient Node<K,V>[] table;  
   //keyset 方法要返回的结果  
   transient Set<Map.Entry<K,V>> entrySet;  
   //map中保存的键值对的数量  
   transient int size;  
   //hashmap 对象被修改的次数  
   transient int modCount;  
   // 容量乘以装在因子所得结果，如果key-value的 数量等于该值，则调用resize方  法，扩大容量，同时修改threshold的值。  
   int threshold;
   //装载因子  
   final float loadFactor;  
```

问题一、HashMap的底层结构是怎样的？
---------------------

hashMap说到底还是hash表，默认的初始化大小为16，且保证为2的n次方！hash表本质上还是采用的数组来构造的！但是在存数据的时候会出现hash冲突，所以为了解决冲突主要出现了两种方式：
1、开放地址法：即当出现冲突后会，寻找下一个空的散列地址，只要数组足够大总能找到空闲的位置，用来存放数据！
2、链地址法：即出现冲突后，把当前位置作为链表扩展为链表，采用头插法插入新的数据！
而HashMap正是采用第二种方式！但是如果冲突很多会出现链表越来越长，所以在JDK8的时候，为了提高效率当一个位置的链表节点大于8，将把链表重新组成红黑树！所以，实际JDK8的数据结构形式是这样的：
当单个链表的节点数<=8的时候：（图片来自于https://blog.csdn.net/fighterandknight/article/details/61624150）
![这里写图片描述](https://img-blog.csdn.net/20180406232515775?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

当单个链表的节点>8的时候：（图片来自于http://www.th7.cn/Program/java/201611/1005180.shtml）

![这里写图片描述](https://img-blog.csdn.net/20180406232538251?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

问题二、HashMap如何保证的2的n次方容量？
------------------------

上文讲到，hashMap的默认容量大小是16，且不管如何初始化，均能保证是2的n次方，但是源码中是怎么实现这一点的呢？这个要看HashMap的构造函数了！
```
public HashMap(int initialCapacity, float loadFactor) {
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " +
                                               initialCapacity);
        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " +
                                               loadFactor);
        this.loadFactor = loadFactor;
        this.threshold = tableSizeFor(initialCapacity);
    }
```
我们看到，我们传入一个initialCapacity，可是最终作用的结果是：
```
this.threshold = tableSizeFor(initialCapacity);
```
所以我们进入tableSizeFor去看看：
```java
static final int tableSizeFor(int cap) {
        int n = cap - 1;
        n |= n >>> 1;
        n |= n >>> 2;
        n |= n >>> 4;
        n |= n >>> 8;
        n |= n >>> 16;
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;
    }
```
   

这几个位移操作的目的就是求得大于当前容量的最小2的n次方！而且，源码的做法真的很巧妙——他通过不断的“无符号右移+或”操作运算，把原数据一步步变成全1的二进制，而我们知道任意数的二进制每位都置1的话就是大于这个数据的最小2的n次方减1！例如： 
13=1101——>1111=15 
这样我们最好只需要+1操作就可以获得我们需要的数了！ 这里解释四个问题：
***1、为什么 int n = cap - 1操作？*** 
这是因为有的时候可能传递进去的cap恰好是2的n次方，加入是16，则如果直接使用此方法操作就会出现等于17，不符合我们所需的结果！
***2、这些位移操作是如何运算的？***
这里引入网上的一个截图，进行讲解！
![这里写图片描述](https://img-blog.csdn.net/2018040711354633?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70) 
所以，可以看到其实就是不断右移把0和1错开，然后在或操作，则不论是0还是1最终的结果都是1，直到所有位置都为1，则右移+或操作将保持结果不变！之所以最终以为16位，是因为最大的是32位，1->2->4->8->16正好可以满足32位最大值（全1）
***3、 return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;***
这句话的解释是这样的——如果n<0,则直接返回1；否则看n是否大于最大值，大于等于最大值则只能取最大值，否则直接取当前值+1即可
***4、为什么返回的结果赋值给this.threshold = tableSizeFor(initialCapacity);***
这个问题需要到下面HashMap存取数据来看！简单来说就是，存数据的时候回再把这个threshold赋值给cap！

问题三、hashMap存取数据的原理是什么？
----------------------
首先要明白，不论是数组节点和链表节点、红黑树节点每个节点存放的是一个Node   &lt;K,V&gt;对象（注意是对象，不是value基本的数据类型值），而Node&lt;K,V&gt;又继承自Map.Entry&lt;K,V&gt;；实际上HashMap底层维护的是一个Node[] table数组，我们称之为Hash桶数组；
```java
static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;     //每个储存元素key的哈希值
        final K key;        //key
        V value;            //value
        Node<K,V> next;     //链表下一个node

        Node(int hash, K key, V value, Node<K,V> next) {
            this.hash = hash;
            ......
        }

        public final K getKey()        { return key; }
        public final V getValue()      { return value; }
        public final String toString() { return key + "=" + value; }
        public final int hashCode() { ...... }
        public final V setValue(V newValue) { ...... }
        public final boolean equals(Object o) { ....... }
    }
```

当存放数据的时候，首先会根据存放数据key计算hashcode，然后再通过Hash算法的后两步运算（高位运算和取模运算，下文有介绍）来定位该键值对的存储位置，有时两个key会定位到相同的位置，表示发生了Hash碰撞。当出现碰撞的时候则通过链地址发生成链表或者红黑树！当然Hash算法计算结果越分散均匀，Hash碰撞的概率就越小，map的存取效率就会越高。首先来看存储位置的计算方法：
**（参见//- - - - - - - - -  - - -//注释标记）**

```java
public V put(K key, V value) {
	//-----------------hash(key)位置计算------------------------//
        return putVal(hash(key), key, value, false, true);
    }
static final int hash(Object key) {
        int h;
	//----------------1、HashCode计算---------------------//
	//----------------2、高位运算-------------------------//
        return (key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16);
    }

final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
//步骤1:判断是否为null或者长度是否为0，如果是则扩容
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
//步骤2：当前数组索引位置没有数据，则直接赋值
	//-------------------3、取模运算------------------------//
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            Node<K,V> e; K k;
//步骤3：如果数组索引位节点已经存在（即key值相等），则直接覆盖
            if (p.hash == hash &&
                ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else if (p instanceof TreeNode)
//步骤4：否则判断该节点是否是红黑树，是的话则直接插入
                e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            else {
//步骤5：否则生成链表，对链表进行插入（尾插法）
                for (int binCount = 0; ; ++binCount) {
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
//步骤6：若链表的长度大于8了，则转换为红黑树
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
//步骤7：在遍历的时候若节点（非数组位首节点）key值相等，同样采取覆盖操作
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    p = e;
                }
            }
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
        ++modCount;
//步骤8：插入后发现大于阈值了（注意是大于阈值，不是大于实际长度）则进行扩容
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }
```

存储位置就是源码中的tab[i]中的i，他的确立包含三步：
![这里写图片描述](https://img-blog.csdn.net/2018040623413140?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
**1、计算HashCode：h = key.hashCode()**
hashcode的计算方法分为以下几类：
①Object类的hashCode
  返回对象的经过处理后的内存地址，由于每个对象的内存地址都不一样，所以哈希码也不一样。这个是native方法，取决于JVM的内部设计，一般是某种C地址的偏移。
②String类的hashCode
  根据String类包含的字符串的内容，根据一种特殊算法返回哈希码，只要字符串的内容相同，返回的哈希码也相同。
③Integer等包装类
  返回的哈希码就是Integer对象里所包含的那个整数的数值，例如Integer i1=new Integer(100)，i1.hashCode的值就是100 。由此可见，2个一样大小的Integer对象，返回的哈希码也一样。
④int，char这样的基础类
  它们不需要hashCode，如果需要存储时，将进行自动装箱操作，计算方法包装类。
**2、高位运算：(h = key.hashCode()) ^ (h >>> 16)**
高位运算的目的就是使得计算出来的hashCode的高16位和低16都能参与到运算当中——因为最终确立位置的是数组的第三步，第三步中n是数组的长度，如果数组长度很小，那么实际和hash参与运算的只是hash值的很低的几位，那么对于32位的hash值来说前面的位数都是无效的，那么举个例子：
Hash01 =1111 1111 1111 1111 1111 1111 1111 0000
Hash02 =1000 1111 1111 1111 1111 1111 1111 0000
假设hash的长度是16位，即n-1 = 1111，你会发现只要最后四位相同，即使前面28位不同，也会发生碰撞，即此时发生碰撞的概率是28/32 = 7/8=87.5%,可见碰撞的概率有多大！如果采用高位运算的方式，将避免此种因素导致的碰撞！
**3、取模运算：i = (n - 1) & hash**
其实上一步已经讲解了这个步骤的作用，但是这里讨论一个问题：

> HashMap的数组长度默认是16且扩容后要为是2的n次方是为什么？

正常情况下，为了防止碰撞发生，常规的设计是把桶的大小设计为素数。相对来说素数导致冲突的概率要小于合数，具体证明可以参考http://blog.csdn.net/liuqiyao_01/article/details/14475159，Hashtable初始化桶大小为11，就是桶大小设计为素数的应用。而之所以设计成2的n次方，一方面已经采取第二步的“高位运算”优化了碰撞的概率发生几率，另一方面就是考虑了这一步：取模运算！这个设计的很巧妙——本来为了定位一个处于n之内的数字作为实际的数据的索引位的方法是拿hash%(n-1)，但是实际代码中我们很忌讳使用乘除法，但是此时我们发现，如果n是2的n次方，那么-1就是n位的全1二级制，则位运算就是取余运算，所以大大提高了效率！
当确定数据的位置后，就开始直接插入数据！（请对照源码进行阅读）

![这里写图片描述](https://img-blog.csdn.net/20180406234203245?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
> 步骤1:   判断是否为null或者长度是否为0，如果是则扩容！ 
> 步骤2：若当前数组索引位置没有数据，则直接赋值；
> 步骤4：否则判断该节点是否是红黑树，是的话则直接插入
> 步骤5：否则生成链表，对链表进行插入 
> 步骤6：若链表的长度大于8了，则转换为红黑树
> 步骤7：在遍历的时候若节点（非数组位首节点）key值相等，同样采取覆盖操作
> 步骤8：插入后发现大于阈值了（注意是大于阈值，不是大于实际长度）则进行扩容

问题四、HashMap的扩容机制是什么？
--------------------

HashMap底层的hash桶数组Node[] table默认的初始化大小length是16，同时有一个默认负载因子Loadfactor=0.75(可以才构造函数中更改)，阈值threshold = length * loadfactor，扩容倍数为原来数组大小长度length的两倍！需要注意的是（这一点存疑，还请读者确认后理解）：

> ***扩容的时候是实际所有的键值对（包括链表节点和红黑树节点）大于threshold就会触发，而不是hash桶数组被占用的大小大于threshold，也就是说即使只使用了两个hash桶数组位置，只要所有的节点数量size>threshold就会触发扩容！***

看源码：

```java
final Node<K,V>[] resize() {
        Node<K,V>[] oldTab = table;
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        int oldThr = threshold;
        int newCap, newThr = 0;
//如果原map容量>0(说明初始化的时候初始了容量或者已经不是出于初始化阶段的扩容)，则容量翻倍，阈值翻倍
        if (oldCap > 0) {
            if (oldCap >= MAXIMUM_CAPACITY) {
                threshold = Integer.MAX_VALUE;
                return oldTab;
            }
            else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                     oldCap >= DEFAULT_INITIAL_CAPACITY)
                newThr = oldThr << 1; // double threshold
        }
//否则说明，原来的容量为0，即初始化阶段！则判断阈值的大小，阈值大于0，直接使用阈值作为新容量，否则使用默认值！
        else if (oldThr > 0) // initial capacity was placed in threshold
            newCap = oldThr;
        else {               // zero initial threshold signifies using defaults
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);
        }
//无论上述条件如何，都要判断新阈值的大小，如果为0则更新新阈值！
        if (newThr == 0) {
            float ft = (float)newCap * loadFactor;
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                      (int)ft : Integer.MAX_VALUE);
        }
        threshold = newThr;
        @SuppressWarnings({"rawtypes","unchecked"})
            Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
        table = newTab;
        if (oldTab != null) {
            for (int j = 0; j < oldCap; ++j) {
                Node<K,V> e;
                if ((e = oldTab[j]) != null) {
                    oldTab[j] = null;
//如果当前数据不为null，则缓存至e，同时置空原索引位，如果该位是数组索引位，则重新计算e的新索引位，并放置！
                    if (e.next == null)
                        newTab[e.hash & (newCap - 1)] = e;
//否则是红黑树，则作红黑树的操作——太麻烦，现在不讲
                    else if (e instanceof TreeNode)
                        ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
//否则是链表，则进行链表重排——这个下面会详细讲述！
                    else { // preserve order
                        Node<K,V> loHead = null, loTail = null;
                        Node<K,V> hiHead = null, hiTail = null;
                        Node<K,V> next;
                        do {
                            next = e.next;
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;
                            }
                            else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);
                        if (loTail != null) {
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }
        return newTab;
    }
```

问题五：扩容后的链表重排机制是什么？
------------------

在JDK7中，采用头插法来进行扩容后的节点重排，但是在JDK8中是尾插法，这个和起初发生冲突后的做法是一致的！正常情况下，由于扩容后的length不一样了，那么每个节点的hash位置也是不一样的，所以理论上应该是全部重新计算一下然后进行重排，但是这种消耗是巨大的！我们来看看JDK8中是怎么实现的——现在拉出来上面的for循环代码来剖析：

```java
{ // preserve order
                        Node<K,V> loHead = null, loTail = null;
                        Node<K,V> hiHead = null, hiTail = null;
                        Node<K,V> next;
                        do {
                            next = e.next;
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;
                            }
                            else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);
                        if (loTail != null) {
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
```

首先，声明了：

```java
Node<K,V> loHead = null, loTail = null;
Node<K,V> hiHead = null, hiTail = null;
```

我们来说明一下重排的机制，就明白这几个变量的用途：在JDK8中，我们认为扩容后只出现两种情况：
1、一种是还保持在原来的链表节点位置上；
2、另一种就是分离到了另一个索引位置（记住是另一个，不是另外好几个）
所以也就相当把当前链表拆分到了两个hash桶索引位，而“巧合的”是，其中一部分保持原位置，另一部分正好是原位置+原Map长度！所以，这里定义这四个变量，就是寻找被分离后的第一节点，找到第一个节点后，则后续节点按照规则直接顺序连接即可，而连接完成后直接把头结点放到相应的位置就完成了所有节点的重排！
![这里写图片描述](https://img-blog.csdn.net/20180406234250816?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

然后同样再来讲一讲，JDK8中为什么可以用**当前位置j**和**当前位置+原大小j + oldCap**作为新的分离位置索引！看下图可以明白这句话的意思，n为table的长度，图（a）表示扩容前的key1和key2两种key确定索引位置的示例，图（b）表示扩容后key1和key2两种key确定索引位置的示例，其中hash1是key1对应的哈希与高位运算结果。
![这里写图片描述](https://img-blog.csdn.net/20180406234401905?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
元素在重新计算hash之后，因为n变为2倍，那么n-1的mask范围在高位多1bit(红色)，因此新的index就会发生这样的变化：
![这里写图片描述](https://img-blog.csdn.net/20180406234426994?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3UwMTE1NTI0MDQ=/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
因此，我们在扩充HashMap的时候，不需要像JDK1.7的实现那样重新计算hash，只需要看看原来的hash值新增的那个bit是1还是0就好了，是0的话索引没变，是1的话索引变成“原索引+oldCap”，这个设计确实非常的巧妙，既省去了重新计算hash值的时间，而且同时，由于新增的1bit是0还是1可以认为是随机的，因此resize的过程，均匀的把之前的冲突的节点分散到新的bucket了。这一块就是JDK1.8新增的优化点。有一点注意区别，JDK1.7中rehash的时候，旧链表迁移新链表的时候，如果在新表的数组索引位置相同，则链表元素会倒置，但是从上图可以看出，JDK1.8不会倒置！
**但是细心的读者发现源码中明明是if ((e.hash & oldCap) == 0)作为区分的，并不是什么n-1&hash啊？其实情况是这样的，这种做法可以通过等于还是不等于0，来判断当前索引位的hash位置是否变化，举例来说：**
16就是10000，和key1(注意：第5位为0)相与结果为0，而和key2(第5位上面为1)就成了16了(！=0)，这个也就是说，如果相与结果为0，即节点的hash小于< oldLength，否则就是大于>oldLength，这个时候就需要把位置增加oldLength！