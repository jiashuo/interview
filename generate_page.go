package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
)

func main() {
	root := flag.String("path", "./java", "路径")
	url := flag.String("url", "https://gitlab.com/jiashuo/interview/-/raw/main", "地址")

	err := filepath.Walk(*root,
		func(path string, f os.FileInfo, err error) error {
			if f == nil {
				return err
			}
			if f.IsDir() {
				return nil
			}
			prefix := path[:len(path)-3]
			fmt.Printf("[\"%s\",\"%s/%s\"],\n", prefix[6:], *url, path)
			return nil
		})

	if err != nil {
		return
	}
}
